


################
# Bomb, the game

A bomb has been set and nobody knows when it will explode.
Each player needs to provide a number each round and if the
bomb exploded the player loses untill only one player is left standing.

-	when the game is started an explanation is displayed
	( additional points for an immersive story ) and the user is
	prompted for the amount of players. 
-	The bomb has to be primed with the amount of players playing.
-	The players take their turns in random order and each round every
	living player needs to take exactly one turn. Display the current player
	and request a number in range [ 1 - 20 ], tick the bomb this amount of times.
-	If the bomb goed off the tick function with throw a Bomb_Explosion exception
-	repeat this process and display the winner if only one player is left.
-	exit program.

Do not edit Bomb.py



################
##### assignment

This assignment consist of three parts
1. Select additional goals, analyse and design application structure
2. Implement basic application
3. Implement additional goals

additional goals:
- robust user input handling with request for correct input. (Check)
- some tests to verify correct application response.
- use player names instead of player 1 etc. (Check)
- restart game option (check) and the option to add or remove players on restart.

1.
# Optional: select additional goals you would like to implement.
# Analyse possible apllication designs and create at least three different designs.
# create a dataflow diagram for each design and describe the pros and cons.
  ( it is up to you how to create the diagram, a scan of a drawing etc. )

	  A very simple diagram: ( you are allowed to use this to compare other designs with )
	  ______________                                         ______________
	  |            | -- correct player amount input     -->  |            |
	  | User input | -- correct current player number   -->  | Bomb Game  |  -- display text          -->  Display
	  |            |                                         |            |                                _________
	  |            |                                         |            |                                |       |
	  |            |                                         |            |  -- arm ( player_amount ) -->  | Bomb  |
	  |            |                                         |            |  -- tick                  -->  |       |
	  |            |                                         |            |                                ---------
	  --------------                                         --------------

	  Adding all additional goals:
	  ______________                                         ______________
	  |            | -- correct player amount input     -->  |            |
	  | User input | -- incorrect player amount input   -->  | Bomb Game  |  -- display text          -->  Display
	  |            | -- correct player name             -->  |            |
	  |            | -- incorrect player name           -->  |            |                                _________
	  |            | -- correct current player number   -->  |            |                                |       |
	  |            | -- incorrect current player number -->  |            |  -- arm ( player_amount ) -->  | Bomb  |
	  |            | -- correct restart game input      -->  |            |  -- reset                 -->  |       |
	  |            | -- incorrect restart game input    -->  |            |  -- tick                  -->  |       |
	  |            | -- correct add player input        -->  |            |                                ---------
	  |            | -- incorrect add player input      -->  |            |
	  |            | -- correct remove player input     -->  |            |
	  |            | -- incorrect remove player input   -->  |            |
	  --------------                                         --------------
	  pros
	  - all code in one class
	  cons
	  - all code in one class
	  - difficult to test

# Pick one design and describe why you want to implement this one.

2.
# Implement your design.
# What did you learn about this design?

3. ( optional )
# Implement the additional goals.
# What did you learn about this design? did you have any problems implementing the additional goals?



################
########### test

test can be run with the command:
python -m unittest discover



################
############ run

application should be run with the command: ( update this when needed )
python bomb_game/Bomb_Game.py
