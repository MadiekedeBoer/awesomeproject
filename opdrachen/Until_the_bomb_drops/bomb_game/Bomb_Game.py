from python_madieke.opdrachen.Until_the_bomb_drops.bomb_game.bomb.Bomb import Bomb
from python_madieke.opdrachen.Until_the_bomb_drops.bomb_game.Player import Player
import random

class Bomb_game():

    def __init__(self):
        self.players_in_the_game = []
        self.bomb = Bomb()
        self.last_player = 0

    def start_game(self):
        print('With how many players do you want to play?')
        self.bomb.player_amount = int(input())
        self.bomb.arm(self.bomb.player_amount)
        # print('Number of players:', self.bomb.player_amount)

        for player_number in range(1, int(self.bomb.player_amount) + 1):
            player = Player()
            print(f'What is the name of Player {player_number}?')
            player.player_name = input()
            player.player_number = player_number
            self.players_in_the_game.append(player)
        print('Lets start te game!')

    def turn_player(self):
        if self.bomb.player_amount >1:
            # print('Players still in the game:', self.bomb.player_amount)
            player_on_turn = random.choice(self.players_in_the_game)
            self.last_player = player_on_turn.player_number
            print('Player on turn:', player_on_turn.player_name)

            print(f'Which number between 1 and 20 do you want to choose {player_on_turn.player_name}?'
                  f'(Type Restart if you want to restart the game)')
            # if input() == 'Restart':
            #     self.bomb.reset()
            #     print('The game is restarted')
            #     print('These players are in the game now:')
            #     print(len(self.players_in_the_game))
            #     # for player in self.players_in_the_game:
            #     #     print(f'Player {player.player_name}')
            #     self.players_in_the_game.clear()
            #     game.start_game()


            # else:
            chosen_number = int(input())
            self.ticks(chosen_number)

        else:
            for winner in self.players_in_the_game:
                print('The winner is:',winner.player_name)

    def ticks(self, chosen_number):
        if chosen_number < 21 and chosen_number > 0:
            for ticks in range(chosen_number):
                self.bomb.tick()
            game.turn_player()
        else:
            print('This number is not between 1 and 20, give a number in this range')
            chosen_number = int(input())
            self.ticks(chosen_number)

    def activation(self):
        try:
            game.turn_player()
        except  Exception as ex:
            print(ex)
        finally:
            if len(self.players_in_the_game) > 1:
                # print('Number of last player', self.last_player)
                for player in self.players_in_the_game:
                    if player.player_number == self.last_player:
                        self.players_in_the_game.remove(player)

                # for player in self.players_in_the_game:
                #     print(player.player_number)

                game.activation()
            else:
                print('End of game')




game = Bomb_game()
game.start_game()
game.activation()