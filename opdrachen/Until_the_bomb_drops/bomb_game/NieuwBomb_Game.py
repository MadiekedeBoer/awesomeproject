import tkinter as Tk
from tkinter import *

class NewBombGame:
    def __init__(self):
        self.root = Tk()
        self.e = Entry(self.root)
        self.number_persons_in_game = 0
        self.list_names = []

    def save_number_persons(self):
        x = self.e.get()
        number_of_persons = int(x)
        self.number_persons_in_game = number_of_persons
        self.root.destroy()
        print(self.number_persons_in_game)
        self.insert_names()

    def begin_game(self):
        myLabel = Label(self.root, text='Whith how many persons do you want to play?')
        myLabel.pack()
        self.e.pack()
        myButton = Button(self.root, text = "Oke", command=self.save_number_persons)
        myButton.pack()
        self.root.mainloop()

    def insert_names(self):
        self.root = Tk()
        for r in range(self.number_persons_in_game):
            Label(self.root, text='Player ' + str(r + 1)).grid(row=r, column=0)
            entry  = Entry(self.root, bd=3)
            entry.grid(row=r, column=1)
            self.list_names.append(entry)
        B = Button(self.root, text = "Enter", command = self.save_names)
        B.place(x=100,y=100)
        self.root.geometry("250x200+10+10")
        self.root.mainloop()

    def save_names(self):
        for name in self.list_names:
            print(name.get())
        self.root.destroy()



game = NewBombGame()
game.begin_game()

