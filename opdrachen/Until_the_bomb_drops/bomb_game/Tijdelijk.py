import tkinter as tk

def onclick():
    print("Button Clicked")

def button():

    root = tk.Tk()
    root.title("GUI Button")

    # First Step: Create Element
    btn1 = tk.Button(root, text="Button 1", command=onclick)
    btn2 = tk.Button(root, text="Button 2", command=onclick)

    # Last Step: Put element on main window
    btn1.pack()
    btn2.pack()

    root.mainloop()

button()

