import random

"""
   !!! DO NOT EDIT THIS CLASS !!!
   Exception class used for bomb explosion event
   !!! DO NOT EDIT THIS CLASS !!!
"""
class Bomb_Explosion( Exception ):
    
    def __str__( self ):
        return "Bomb goes BOOM"


"""
   !!! DO NOT EDIT THIS CLASS !!!
   Bomb class, can be initialised for x amount of players and will raise Bomb_Explosion Exception
   when it explodes
   !!! DO NOT EDIT THIS CLASS !!!
"""
class Bomb():

    def __init__( self ):
        """
           initialise bomb with zero players ( every tick goes BOOM )
        """
        self.player_amount = 0
        self.bomb_count = 0

    def arm( self, player_amount ):
        """
           Arm the bomb for a game with player_amount of players.
           player_amount will be stored and used on reset
        """

        self.player_amount = player_amount
        self.reset()

    def reset( self ):
        """
           Random bomb_count is set based of amount of players.
        """
        self.bomb_count = random.randrange( self.player_amount * 2, self.player_amount * 5 )

    def tick( self ):
        """
           One bomb_count will be removed if it reaches 0 the bomb goed BOOM and raises
           Bomb_Explosion Exception.

           player_amount will be reduced by one and bomb_count will be reset to an appropriate
           amount
        """
        self.bomb_count -= 1
        if( self.bomb_count <= 0 ):
            self.player_amount -= 1
            self.reset()
            raise Bomb_Explosion