import random
import unittest
from bomb.Bomb import Bomb, Bomb_Explosion

class Test_Bomb( unittest.TestCase ):

    def test_bomb_explodes( self ):

        # bomb_count will be 24
        random.seed( 1 ) 

        # init bomb with 10 players
        bomb = Bomb()
        bomb.arm( 10 )

        # verify bomb count and player count
        self.assertTrue( bomb.bomb_count == 24 )
        self.assertTrue( bomb.player_amount == 10 )

        # first 23 ticks the bomb should not explode
        for _ in range( 23 ):
            bomb.tick()

        try:
            bomb.tick()
            # bomb should raise error and never reach this line
            self.fail( "Bomb did not explode" )
        except Bomb_Explosion as err:
            self.assertTrue( str( err ) == "Bomb goes BOOM" )

        # verify bomb count and player count
        self.assertTrue( bomb.bomb_count == 36 )
        self.assertTrue( bomb.player_amount == 9 )





        
if __name__ == '__main__':
    unittest.main()